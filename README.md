# thunderbird

mail/news client with RSS, chat and integrated spam filter support. https://thunderbird.net

# Some Add-ons
* [CardBook](https://addons.thunderbird.net/en-US/thunderbird/addon/cardbook/)